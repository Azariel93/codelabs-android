package fr.mds.codelabsandroid;

import androidx.appcompat.app.AppCompatActivity;
import fr.mds.codelabsandroid.activities.codelab_4_1.DroidCafe;
import fr.mds.codelabsandroid.activities.codelab_4_2.DroidCafe2;
import fr.mds.codelabsandroid.activities.codelab_4_2.KeyboardDialPhone;
import fr.mds.codelabsandroid.activities.codelab_4_3.CodingChall1;
import fr.mds.codelabsandroid.activities.codelab_4_3.DialogForAlert;
import fr.mds.codelabsandroid.activities.codelab_4_3.DroidCafe3;
import fr.mds.codelabsandroid.activities.codelab_4_3.PickerForDate;
import fr.mds.codelabsandroid.activities.codelab_5_1.BatteryActivity;
import fr.mds.codelabsandroid.activities.codelab_5_1.Scorekeeper;
import fr.mds.codelabsandroid.activities.codelab_5_2.HelloSharedPrefs;
import fr.mds.codelabsandroid.activities.codelab_5_2.MaterialMe_Starter;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private String TAG = "fr.mds.codelabsandroid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void launchActivity4_1(View view) {
        Intent intent = new Intent(this, DroidCafe.class);
        startActivity(intent);
    }

    public void launchActivity4_2(View view) {
        Intent intent = new Intent(this, DroidCafe2.class);
        startActivity(intent);
    }

    public void KeyboardDialPhone(View view) {
        Intent intent = new Intent(this, KeyboardDialPhone.class);
        startActivity(intent);
    }

    public void launchActivity4_3(View view) {
        Intent intent = new Intent(this, DroidCafe3.class);
        startActivity(intent);
    }

    public void launchActivity4_chall_1(View view) {
        Intent intent = new Intent(this, CodingChall1.class);
        startActivity(intent);
    }

    public void launchActivity4_4(View view) {
        Intent intent = new Intent(this, DialogForAlert.class);
        startActivity(intent);
    }

    public void launchActivity4_5(View view) {
        Intent intent = new Intent(this, PickerForDate.class);
        startActivity(intent);
    }

    public void launchActivity5_1(View view) {
        Intent intent = new Intent(this, Scorekeeper.class);
        startActivity(intent);
    }

    public void launchActivity5_1_chall(View view) {
        Intent intent = new Intent(this, BatteryActivity.class);
        startActivity(intent);
    }

    public void launchActivity5_2_hello_shared_prefs(View view) {
        Intent intent = new Intent(this, HelloSharedPrefs.class);
        startActivity(intent);
    }

    public void launchActivity5_2_material_me(View view) {
        Intent intent = new Intent(this, MaterialMe_Starter.class);
        startActivity(intent);
    }
}

package fr.mds.codelabsandroid.activities.codelab_4_1;

import androidx.appcompat.app.AppCompatActivity;
import fr.mds.codelabsandroid.R;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class OrderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        Intent intent = getIntent();
        String message = "Order: " +
                intent.getStringExtra(DroidCafe.EXTRA_MESSAGE);
        TextView order_textView = findViewById(R.id.order_textview);
        order_textView.setText(message);
    }
}

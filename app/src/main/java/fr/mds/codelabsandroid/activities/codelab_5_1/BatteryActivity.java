package fr.mds.codelabsandroid.activities.codelab_5_1;

import androidx.appcompat.app.AppCompatActivity;
import fr.mds.codelabsandroid.MainActivity;
import fr.mds.codelabsandroid.R;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class BatteryActivity extends AppCompatActivity {

    private int batterySize = 0;

    private ImageView imgBattery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battery);

        imgBattery = (ImageView)findViewById(R.id.batteryImg);

        setImage();
    }

    public void setImage() {
        int imgRes = 0;
        switch(batterySize) {
            case 6:
                imgRes = R.drawable.ic_battery_full;
                break;
            case 5:
                imgRes = R.drawable.ic_battery_90;
                break;
            case 4:
                imgRes = R.drawable.ic_battery_80;
                break;
            case 3:
                imgRes = R.drawable.ic_battery_60;
                break;
            case 2:
                imgRes = R.drawable.ic_battery_50;
                break;
            case 1:
                imgRes = R.drawable.ic_battery_30;
                break;
            default:
                imgRes = R.drawable.ic_battery_20;
                break;
        }
        imgBattery.setImageResource(imgRes);
    }

    public void increaseBattery(View view) {
        if (batterySize < 6) {
            batterySize++;
            setImage();
        }
    }

    public void decreaseBattery(View view) {
        if (batterySize > 0) {
            batterySize --;
            setImage();
        }
    }

    public void Home(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}

package fr.mds.codelabsandroid.activities.codelab_4_3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import fr.mds.codelabsandroid.MainActivity;
import fr.mds.codelabsandroid.R;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class PickerForDate extends AppCompatActivity {

    private String TAG = "fr.mds.codelabsandroid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picker_for_date);
    }

    public void showDatePicker(View view) {
        Log.d(TAG, "showDatePicker: ");
        DialogFragment newFragment = new DatePickerFragment();
        Log.d(TAG, "showDatePicker: ");
        newFragment.show(getSupportFragmentManager(), getString(R.string.datepicker));
    }

    public void processDatePickerResult(int year, int month, int day) {
        String month_string = Integer.toString(month + 1);
        String day_string = Integer.toString(day);
        String year_string = Integer.toString(year);
        String dateMessage = (month_string +
                "/" + day_string +
                "/" + year_string);

        Toast.makeText(this, getString(R.string.date) + dateMessage,
                Toast.LENGTH_SHORT).show();
    }

    public void Home(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
